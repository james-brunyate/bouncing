from math import exp
import matplotlib.pyplot as plt
import numpy as np

def f(x, t):
	return x

x0 = 1
errors = []
h_list = []

def euler(f, x1, t1, h):
	x2 = x1 + h*f(x1, t1)
	return x2

def solve_to(x1, t1, t_fin, h):
	t = 0; x = x1; t_fin = 1;
	while t < t_fin:
		h = min(t_fin, t + h) - t
		x = euler(f, x, t, h)
		t = t + h
	return x

for i in range(10):
	h = 10**(-i)
	h_list.append(h)
	e_num = solve_to(x0, 0, 1, h)
	e_true = exp(1)
	err = abs(e_num - e_true)
	errors.append(err)
	print("Step size:", h, "Error:", err)

plt.figure()
plt.subplot(222)
plt.plot(h_list, errors)
plt.yscale('log')
plt.title('Log graph of errors as h changes')
plt.grid(True)

